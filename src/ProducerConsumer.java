import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class ProducerConsumer 
{
	public static void main(String args[]) throws Exception
	{
	     BlockingQueue<Character> sharedQueue = new LinkedBlockingQueue<Character>();
	     BufferedReader br = new BufferedReader( new InputStreamReader(System.in));
	     
	     System.out.print("Input the string : ");
	     String word = br.readLine();
	     System.out.print("Output: ");
	     
	     Thread prodThread = new Thread(new Producer(sharedQueue, word));
	     
	     prodThread.start();
	}
}

class Producer extends Thread 
{

    private BlockingQueue<Character> sharedQueue;
    private String word;
    
    public Producer(BlockingQueue<Character> sharedQueue, String w) 
    {
        this.sharedQueue = sharedQueue;
        this.word = w;
    }

    @Override
    public void run() 
    {
        for(int i=0; i<word.length(); i++)
        {
            try 
            {
                sharedQueue.put(word.charAt(i));
                new Thread(new Consumer(sharedQueue)).start();
                sleep(40);
            } 
            catch (Exception exc) 
            {
            	System.err.println("Error: " + exc.getMessage());
            }
        }
    }

}


class Consumer extends Thread
{

    private BlockingQueue<Character> sharedQueue;

    public Consumer (BlockingQueue<Character> sharedQueue) 
    {
        this.sharedQueue = sharedQueue;
    }
  
    @Override
    public void run() 
    {
        try 
        {
            System.out.print(sharedQueue.take());
        } 
        catch (Exception exc) 
        {
        	System.err.println("Error: " + exc.getMessage());
        }
        
    }


}
